//TODO: hacer esto
//FIXME: revisar código


'use strict'

const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const port = process.env.PORT || 3001

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.get('/api/product', (req, res) => {
  console.log(req.body)
  res.status(200).send('Listado de productos')
})
app.post('/api/product', (req, res) => {
  var cuerpo = req.body
  console.log(cuerpo)
  res.status(200).send(`Producto "${req.body.name}" insertado `)
})

app.listen(port, () => {
  console.log('APi REst corriendo en http://localhost:3001')
})
