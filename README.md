# Ejemplo de codigo
por que no nos
vamos
> *esto es una cita*


*y esto cursiva*

**y esto negrita**

`<img src="imagen"></img>`

```[javascript]
var nuevoValor = 3;
//esto sigue siendo codigo
<img src="imagen"></img>
```

## un nivel 2 de título

que puede requerir un tercero

### tercero

#### cuarto

* que
* es esto

1. que
2. es
3. est

[enlace](http://enlace.com "el enlace")

![texto de foto alternativo](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Haroopad_Markdown_editor.png/220px-Haroopad_Markdown_editor.png "texto de foto alt")







